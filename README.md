
# WKCookieExtension
An extension to inject and delete cookie

## Getting Started
These instructions will get you a copy of the project up and running on your local machine. Please see ``tag`` to see list of available version.

### Prerequisites
* Currently support iOS 11 and above
* Swift version 3.0 and above

### Installing Dependencies
To integrate this module to your apps, please follow these step


1. Navigate to your project and in the ****Swift Package**** section click the ****+**** button. ![Screenshot](https://raw.githubusercontent.com/radityakurnianto/screenshots/master/Screen%20Shot%202020-05-19%20at%207.34.09%20PM.png)

2. On the next step input [this project git URL](https://bitbucket.org/radityakurnianto/wkcookieextension/src/master/) to the search field, then click ****Next****. ![Screenshot](https://raw.githubusercontent.com/radityakurnianto/screenshots/master/Screen%20Shot%202020-05-19%20at%207.19.58%20PM.png)

3. Under ****Rules**** options you can specify what *_version,  branch, or commit_* you could use then click ****Next****. ![Screenshot](https://github.com/radityakurnianto/screenshots/blob/master/Screen%20Shot%202020-05-19%20at%207.20.21%20PM.png?raw=true)

4. You will see module name and clikc ****Finish**** to use module in your project ![Screenshot](https://github.com/radityakurnianto/screenshots/blob/master/Screen%20Shot%202020-05-19%20at%207.43.04%20PM.png?raw=true)

5. When xcode complete adding module, you will see the module under Swift Package Dependencies, and you are done.

  

![Screenshot](https://github.com/radityakurnianto/screenshots/blob/master/Screen%20Shot%202020-05-19%20at%207.44.56%20PM.png?raw=true)

### Intializing webview
Please provide the following code when you setup a ``WKWebView``.
```swift
lazy  var  webview: WKWebView = {
    // BEGIN required setup
    let contentController = WKUserContentController()
    let configuration = WKWebViewConfiguration()
    let dataStore = WKWebsiteDataStore.nonPersistent()

    configuration.userContentController = contentController
    configuration.websiteDataStore = dataStore
    // END required setup
    
    let webView: WKWebView = WKWebView(frame: self.view.bounds, configuration: configuration)
    webView.navigationDelegate = self

    return webView
}()
```

### Injecting cookie
Cookie injection must take an array of ``[HTTPCookie]`` and ``URL``. This method will give you a callback with a reference to the current ``webview`` and ``URLRequest``

```swift
import WKCookieExtensions

self.webView.inject(cookies: [cookie1, cookie2], url: url) { [weak self] (webview, urlRequest) in
    self?.webView.load(urlRequest)
}

```

### Delete cookie
Deleting cookie is very simple. You just need to provide an array of ``[HTTPCookie]`` and ``URL``. This method will give you a callback with a reference to the current ``webview`` the you can just call load 
```swift
import WKCookieExtensions

self.webView.delete(cookies: [cookie], url: url) { [weak self] (webview, request) in
    self?.webView.load(URLRequest(url: url))
}
```
