//
//  WKWebView+Cookie.swift
//  TestWebKit
//
//  Created by Raditya Kurnianto on 5/19/20.
//  Copyright © 2020 Detikcom. All rights reserved.
//

import WebKit

@objc public extension WKWebView {
    @objc func inject(cookies: [HTTPCookie], url: URL, completion: @escaping ((WKWebView, URLRequest)->Void)) -> Void {
        if #available(iOS 11.0, *) {
            HTTPCookieStorage.shared.cookieAcceptPolicy = .always
            
            cookies.forEach { (cookie) in
                HTTPCookieStorage.shared.setCookie(cookie)
            }
            
            let storedCookies = HTTPCookieStorage.shared.cookies ?? []
            
            let dataStore = WKWebsiteDataStore.nonPersistent()
            
            let waitGroup = DispatchGroup()
            storedCookies.forEach { (cookie) in
                print("[cookie] Cookie: \(cookie)")
                waitGroup.enter()
                self.configuration.websiteDataStore.httpCookieStore.setCookie(cookie, completionHandler: {
                    waitGroup.leave()
                    print("[cookie] Cookie has been set")
                })
            }
            
            waitGroup.notify(queue: DispatchQueue.main) {
                self.configuration.websiteDataStore = dataStore
                completion(self, URLRequest(url: url))
            }
        } else {
            // process cookie for iOS 10
            
        }
    }
    
    @objc func delete(cookies: [HTTPCookie], url: URL, completion: @escaping ((WKWebView, URLRequest)->Void)) -> Void {
        if #available(iOS 11.0, *) {
            let storedCookies = HTTPCookieStorage.shared.cookies ?? []
            let dataStore = WKWebsiteDataStore.nonPersistent()
            
            let waitGroup = DispatchGroup()
            storedCookies.forEach { (cookie) in
                print("[cookie] delete Cookie: \(cookie)")
                waitGroup.enter()
                self.configuration.websiteDataStore.httpCookieStore.delete(cookie) {
                    HTTPCookieStorage.shared.deleteCookie(cookie)
                    waitGroup.leave()
                    print("[cookie] delete Cookie has been delete")
                }
            }
            
            waitGroup.notify(queue: DispatchQueue.main) {
                self.configuration.websiteDataStore = dataStore
                self.printCookie()
                completion(self, URLRequest(url: url))
            }
        }
    }
    
    @available(iOS 11.0, *)
    private func printCookie() {
        self.configuration.websiteDataStore.httpCookieStore.getAllCookies { (cookies) in
            cookies.forEach {
                print($0)
            }
        }
    }
}

public extension HTTPCookie {
    func toString() -> String {
        return "\(self.name)=\(self.value);"
    }
}
