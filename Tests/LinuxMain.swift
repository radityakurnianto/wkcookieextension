import XCTest

import WKCookieExtensionTests

var tests = [XCTestCaseEntry]()
tests += WKCookieExtensionTests.allTests()
XCTMain(tests)
